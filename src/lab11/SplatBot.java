package lab11;

import splatbot.Action;
import splatbot.Cell;
import splatbot.SplatBotColor;
import splatbot.Splatter;

public class SplatBot {
	private Cell[][] surve;
	private int turns = 0;

	public static void main(String[] args) {
		int delay = 50;
		int nTurns = 500;
		new Splatter(splatbot.RobotLefty.class, lab11.SplatBot.class, delay, nTurns);
	}

	public SplatBot(SplatBotColor color) {

	}

	public void survey(Cell[][] surv) {
		surv = surve;
	}

	public Action getAction(Cell Left, Cell Front, Cell Right) {
		turns++;
		if (turns == 1 || turns % 15 == 0)
			return Action.SURVEY;

		else if (Front == Cell.RED || Front == Cell.NEUTRAL)
			return Action.MOVE_FORWARD;

		else if (Right == Cell.RED)
			return Action.TURN_RIGHT;
		
		else if (Left == Cell.RED)
			return Action.TURN_LEFT;

		else if (Left == Cell.NEUTRAL)
			return Action.TURN_LEFT;
		
		else if (Right == Cell.NEUTRAL)
			return Action.TURN_RIGHT;
		
		else if (Front == Cell.WALL && Right == Cell.WALL)
			return Action.TURN_LEFT;
		
		else if (Left == Cell.WALL && Front == Cell.WALL)
			return Action.TURN_RIGHT;
		
		else if (Right == Cell.BLUE && Front == Cell.BLUE && Left == Cell.BLUE)
			return Action.MOVE_FORWARD;
		
		else if (Right == Cell.WALL && Front == Cell.BLUE && Left == Cell.ROCK)
			return Action.MOVE_FORWARD;
		
		else if (Right == Cell.BLUE && Front == Cell.BLUE && Left == Cell.WALL)
			return Action.MOVE_FORWARD;
		
		else if (Right == Cell.ROCK && Front == Cell.BLUE && Left == Cell.BLUE)
			return Action.MOVE_FORWARD;
		
		else if (Front == Cell.RED_ROBOT)
			return Action.SPLAT;

		else if (Left==Cell.WALL&&Front==Cell.ROCK)
			return Action.TURN_RIGHT;
		
		else if (Right==Cell.WALL&&Front==Cell.ROCK)
			return Action.TURN_LEFT;
		
		else if (Front==Cell.WALL&&Right==Cell.ROCK&&Left==Cell.ROCK)
			return Action.MOVE_BACKWARD;
		
		else if (Front ==Cell.ROCK&&Left==Cell.BLUE&&Right==Cell.WALL)
			return Action.TURN_LEFT;
		
		else if (Front ==Cell.ROCK&&Right==Cell.BLUE&&Left==Cell.BLUE)
			return Action.TURN_RIGHT;
		
		else if (Right == Cell.BLUE && Front == Cell.ROCK && Left == Cell.WALL)
			return Action.TURN_RIGHT;
		
		else if (Right == Cell.BLUE && Front == Cell.ROCK && Left==Cell.BLUE)
			return Action.TURN_RIGHT;
	
		else if (Front == Cell.ROCK || Front == Cell.BLUE || Front == Cell.WALL)
			return Action.TURN_LEFT;
		
		else if (Left == Cell.ROCK || Left == Cell.BLUE || Left == Cell.WALL)
			return Action.TURN_RIGHT;
		
		else if (Right == Cell.ROCK || Right == Cell.BLUE || Right == Cell.WALL)
			return Action.MOVE_FORWARD;

		else if (Front == Cell.WALL)
			return Action.TURN_RIGHT;
		
		else if (Front == Cell.ROCK)
			return Action.TURN_LEFT;
	
		else
			return Action.MOVE_FORWARD;

	}
}
