package hw4;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;

public class Main {
	public static void main(String[] args) throws IOException {
		JFileChooser dialog = new JFileChooser();
		int status = dialog.showOpenDialog(null);
		if (status == JFileChooser.APPROVE_OPTION) {
			File file = dialog.getSelectedFile();
			BufferedImage src = ImageIO.read(file);
			BufferedImage idk = ImageUtilities.makeSeamless(src,5);
			BufferedImage dst = ImageUtilities.tile(src,10,10);
			ImageIO.write(src, FilenameUtilities.getExtension(file), FilenameUtilities.appendToName(file, "swapped"));
			
		}
	}
}
