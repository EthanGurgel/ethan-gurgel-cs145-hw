package hw4;

import java.io.File;

public class FilenameUtilities {
	public static String getExtension(File f) {
		String s = f.getName();
		if (s.lastIndexOf('.') > 0) {
			String t = s.substring(s.lastIndexOf('.') + 1);
			return t;
		} else

			return null;

	}

	public static File appendToName(File f, String s) {
		String str = f.getName();
		String strin = "";
		if (str.indexOf('.') > 0) {

			strin = str.substring(0, str.indexOf('.')) + "_" + s + str.substring(str.indexOf('.'));
		} else {
			strin = str + "_" + s;
		}
		return new File(f.getParentFile(), strin);
	}
}