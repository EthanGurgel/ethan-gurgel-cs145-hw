package hw4;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageUtilities {

	public static BufferedImage swapCorners(BufferedImage image) throws IOException {
		// image = ImageIO.read(new File("C:/Users/Ethan/Desktop/Destiny
		// (5).png"));
		int w = image.getWidth();
		int h = image.getHeight();
		BufferedImage image1 = new BufferedImage(w, h, image.getType());

		for (int c = 0; c < w; ++c) {
			for (int r = 0; r < h; ++r) {

				int origcol = image.getRGB((c + (w / 2)) % w, (r + (h / 2)) % h);
				image1.setRGB(c, r, origcol);
			}
		}
		// ImageIO.write(image1, "png", new
		// File("C:/Users/Ethan/Desktop/testing.png"));
		return image1;

	}

	public static BufferedImage getCircleMask(int w, int h, double power) {
		BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_BYTE_GRAY);
		double midC = w / 2.0;
		double midR = h / 2.0;
		double radius = Math.min(midC, midR);
		for (int c = 0; c < w; ++c) {
			for (int r = 0; r < h; ++r) {
				double dist = Math.sqrt((Math.pow(c - midC, 2)) + (Math.pow(r - midR, 2)));
				double normDistance = dist / radius;
				double tweakedDist = Math.pow(normDistance, power);
				float gray = (float) (1 - tweakedDist);
				if (dist >= radius) {
					image.setRGB(c, r, 0);
				} else {
					Color newC = new Color(gray, gray, gray);

					int newRGB = newC.getRGB();
					image.setRGB(c, r, newRGB);
				}
			}
		}
		return image;
	}

	public static Color mix(Color one, Color two, double weight) {

		int r1 = one.getRed();
		int b1 = one.getBlue();
		int g1 = one.getGreen();
		int r2 = two.getRed();
		int b2 = two.getBlue();
		int g2 = two.getGreen();
		int newred = mix(r1, r2, weight);
		int newblue = mix(b1, b2, weight);
		int newgreen = mix(g1, g2, weight);
		Color mixer = new Color(newred, newgreen, newblue);
		return mixer;

	}

	public static int mix(int a, int b, double c) {
		double newt = c * a + (1 - c) * b;

		return (int) newt;

	}

	public static BufferedImage addMasked(BufferedImage image1, BufferedImage gray1, BufferedImage image2,
			BufferedImage gray2) {
		BufferedImage melt1 = new BufferedImage(image1.getWidth(), image1.getHeight(), image1.getType());
		for (int c = 0; c < melt1.getWidth(); c++) {
			for (int r = 0; r < melt1.getHeight(); r++) {
				Color rgb = new Color(image1.getRGB(c, r));
				Color rgbmasked = new Color(gray1.getRGB(c, r));
				Color rgb2 = new Color(image2.getRGB(c, r));
				Color rgbmaksed2 = new Color(gray2.getRGB(c, r));

				int Aweight = rgbmasked.getRed();
				int Bweight = rgbmaksed2.getRed();
				double summedweight = Aweight + Bweight;
				double weight = 0.0;
				if (summedweight == 0) {
					weight = 0.5;
				} else {
					weight = Aweight / summedweight;
				}
				Color mixtape =mix(rgb, rgb2, weight);
				int total = mixtape.getRGB();

				melt1.setRGB(c, r, total);

			}
		}
		return melt1;
	}

	public static BufferedImage tile(BufferedImage image, int x, int y) {
		int h = image.getHeight();
		int w = image.getWidth();
		int tilew = w * x;
		int tileh = h * y;
		BufferedImage tiled = new BufferedImage(tilew, tileh, BufferedImage.TYPE_INT_RGB);
		for (int c = 0; c < tilew; c++) {
			for (int r = 0; r < tileh; r++) {
				int picked = image.getRGB(c % w, r % h);
				tiled.setRGB(c, r, picked);
			}
		}
		return tiled;
	}

	public static BufferedImage makeSeamless(BufferedImage image, double power) throws IOException {
		BufferedImage ihatethisassignment = swapCorners(image);
		BufferedImage cirlcemasked= getCircleMask(ihatethisassignment.getWidth(),ihatethisassignment.getHeight(), power);
		BufferedImage swapperoo= swapCorners(cirlcemasked);
		BufferedImage maske1=addMasked(image,cirlcemasked,ihatethisassignment,swapperoo);
		
		return maske1;
		
	}
}