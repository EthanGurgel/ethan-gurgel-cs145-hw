package hw1;

import java.util.Scanner;

public class LemonAid {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		System.out.print("How many parts lemon juice? ");
		int juice= in.nextInt();
		System.out.print("How many parts sugar? ");
		int sug= in.nextInt();
		System.out.print("How many parts water? ");
		int h20= in.nextInt();
		System.out.print("How many cups of lemonade? ");
		int cups= in.nextInt();
		double parts=juice+sug+h20;
		double juice2=cups*juice/parts;
		double sug2= cups*sug/parts;
		double h202= cups*h20/parts;
		System.out.println("Amounts (in cups):");
		System.out.println("  Lemon juice: "+ juice2);
		System.out.println("  Sugar: "+ sug2);
		System.out.println("  Water: "+ h202);
		
	}
}




