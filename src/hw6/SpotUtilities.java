package hw6;

import java.io.File;

import hw6.speccheck.GifSequenceWriter;

public class SpotUtilities {

	public static int[] countNeighbors(boolean[][] bitmap, int horizRadiusOuter, int vertRadiusOuter,
			int horizRadiusInner, int vertRadiusInner, int columnIndex, int rowIndex) {
		int[] frog = new int[2];
		int activator = 0;
		int inhibitor = 0;

		for (int r = rowIndex - vertRadiusOuter; r <= rowIndex + vertRadiusOuter; r++) {
			for (int c = columnIndex - horizRadiusOuter; c <= columnIndex + horizRadiusOuter; c++) {
				int rDelta = Math.abs(r - rowIndex);
				int cDelta = Math.abs(c - columnIndex);
				double outerDistance = (Math.pow(cDelta, 2) / Math.pow(horizRadiusOuter, 2))
						+ (Math.pow(rDelta, 2)) / (Math.pow(vertRadiusOuter, 2));
				double innerDistance = (Math.pow(cDelta, 2) / Math.pow(horizRadiusInner, 2))
						+ (Math.pow(rDelta, 2)) / (Math.pow(vertRadiusInner, 2));
				if (innerDistance <= 1 && outerDistance <= 1 && BitmapUtilities.isOn(bitmap, c, r)) {
					activator++;
				} else if (outerDistance <= 1 && BitmapUtilities.isOn(bitmap, c, r)) {
					inhibitor++;
				}
			}
		}
		frog[0] = activator;
		frog[1] = inhibitor;
		return frog;
	}

	public static boolean[][] step(boolean[][] bitmap, int horizRadiusOuter, int vertRadiusOuter, int horizRadiusInner,
			int vertRadiusInner, double prop) {
		boolean[][] output = new boolean[bitmap.length][bitmap[0].length];
		for (int r = 0; r < bitmap.length; r++) {
			for (int c = 0; c < bitmap[r].length; c++) {
				int[] arr = countNeighbors(bitmap, horizRadiusOuter, vertRadiusOuter, horizRadiusInner, vertRadiusInner,
						c, r);
				double difference = arr[0] - prop * arr[1];
				if (Math.abs(difference) < .001) {
					output[r][c] = bitmap[r][c];
				} else if (difference > 0) {
					output[r][c] = true;
				} else {
					output[r][c] = false;
				}
			}
		}
		return output;
	}

	public static int converge(boolean[][] bitmap, int horizRadiusOuter, int vertRadiusOuter, int horizRadiusInner,
			int vertRadiusInner, double prop, File f, int maxSteps) {
		boolean[][] pre = bitmap;
		boolean[][] post = BitmapUtilities.create(bitmap.length,bitmap[0].length);
		int count = 0;
		GifSequenceWriter out = new GifSequenceWriter(f, 200, true);
		out.appendFrame(BitmapUtilities.toBufferedImage(bitmap));
		boolean yes = true;
		while (count < maxSteps && yes) {
			post = step(pre, horizRadiusOuter, vertRadiusOuter, horizRadiusInner, vertRadiusInner, prop);
			out.appendFrame(BitmapUtilities.toBufferedImage(post));
			yes= !BitmapUtilities.equals(pre, post);
			pre = post;
			count++;
		}
		out.close();
		return count +1;
	}
}
