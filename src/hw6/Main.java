package hw6;

import hw6.speccheck.SpotDesigner;

public class Main {
public static void main(String[] args) {
	boolean[][] bitmapper = BitmapUtilities.create(250, 250);
	BitmapUtilities.randomize(bitmapper, 35136);
	SpotDesigner.showNew(128, 128, 26, 20,
			10, 15, 0.35, 100, (short) 3249, 200);
	//converge(bitmapper, 20,20, 15,5,);
}
}
