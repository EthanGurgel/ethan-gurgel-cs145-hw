package hw2;

public class NumberUtilities {

	public static void main(String[] args) {

		getGameCount(2);
		getFraction(5.600);
	}

	public static int round10(double x) {
		int p = (int) (Math.round(x / 10.0) * 10);
		System.out.println(p);
		return p;
	}

	public static int getGameCount(int rounds) {
		int games = (int) (Math.pow(2, rounds) - 1);
		System.out.println(games);
		return games;
	}

	public static double getFraction(double frac) {
		String fini=Double.toString(frac);
		String tapu=fini.substring(fini.indexOf('.'));
		Double koko=Double.parseDouble(tapu);
		return koko;


	}
}
