package hw2;

public class BrailleUtilities {
	public static final String RAISED = "\u2022";
	public static final String UNRAISED = "\u00b7";
	public static final String WORD_SPACER = "    ";
	public static final String LETTER_SPACER = "  ";

	public static void main(String[] args) {
<<<<<<< HEAD
		System.out.println(translate("derek"));
=======

		translate("the quick brown fox");
>>>>>>> branch 'master' of https://EthanGurgel@bitbucket.org/EthanGurgel/ethan-gurgel-cs145-hw.git

	}

	public static String translate(String transL8) {
		String topper=translateTopLine(transL8);
		String middler=translateMiddleLine(transL8);
		String bottomer=translateBottomLine(transL8);
		
		return String.format("%s%n%s%n%s%n",topper,middler,bottomer);
	}

	public static String translateTopLine(String top) {
		String tep = top.toLowerCase();
		String tup = tep.replaceAll(" ", WORD_SPACER);
		String a = tup.replaceAll("a", RAISED + UNRAISED + LETTER_SPACER);
		String b = a.replaceAll("b", RAISED + UNRAISED + LETTER_SPACER);
		String c = b.replaceAll("c", RAISED + RAISED + LETTER_SPACER);
		String d = c.replaceAll("d", RAISED + RAISED + LETTER_SPACER);
		String e = d.replaceAll("e", RAISED + UNRAISED + LETTER_SPACER);
		String f = e.replaceAll("f", RAISED + RAISED + LETTER_SPACER);
		String g = f.replaceAll("g", RAISED + RAISED + LETTER_SPACER);
		String h = g.replaceAll("h", RAISED + UNRAISED + LETTER_SPACER);
		String i = h.replaceAll("i", UNRAISED + RAISED + LETTER_SPACER);
		String j = i.replaceAll("j", UNRAISED + RAISED + LETTER_SPACER);
		String k = j.replaceAll("k", RAISED + UNRAISED + LETTER_SPACER);
		String l = k.replaceAll("l", RAISED + UNRAISED + LETTER_SPACER);
		String m = l.replaceAll("m", RAISED + RAISED + LETTER_SPACER);
		String n = m.replaceAll("n", RAISED + RAISED + LETTER_SPACER);
		String o = n.replaceAll("o", RAISED + UNRAISED + LETTER_SPACER);
		String p = o.replaceAll("p", RAISED + RAISED + LETTER_SPACER);
		String q = p.replaceAll("q", RAISED + RAISED + LETTER_SPACER);
		String r = q.replaceAll("r", RAISED + UNRAISED + LETTER_SPACER);
		String s = r.replaceAll("s", UNRAISED + RAISED + LETTER_SPACER);
		String t = s.replaceAll("t", UNRAISED + RAISED + LETTER_SPACER);
		String u = t.replaceAll("u", RAISED + UNRAISED + LETTER_SPACER);
		String v = u.replaceAll("v", RAISED + UNRAISED + LETTER_SPACER);
		String w = v.replaceAll("w", UNRAISED + RAISED + LETTER_SPACER);
		String x = w.replaceAll("x", RAISED + RAISED + LETTER_SPACER);
		String y = x.replaceAll("y", RAISED + RAISED + LETTER_SPACER);
		String z = y.replaceAll("z", RAISED + UNRAISED + LETTER_SPACER);
		// change
		return z.trim();
	}

	public static String translateMiddleLine(String top) {
		String tep = top.toLowerCase();
		String tup = tep.replaceAll(" ", WORD_SPACER);
		String a = tup.replaceAll("a", UNRAISED + UNRAISED + LETTER_SPACER);
		String b = a.replaceAll("b", RAISED + UNRAISED + LETTER_SPACER);
		String c = b.replaceAll("c", UNRAISED + UNRAISED + LETTER_SPACER);
		String d = c.replaceAll("d", UNRAISED + RAISED + LETTER_SPACER);
		String e = d.replaceAll("e", UNRAISED + RAISED + LETTER_SPACER);
		String f = e.replaceAll("f", RAISED + UNRAISED + LETTER_SPACER);
		String g = f.replaceAll("g", RAISED + RAISED + LETTER_SPACER);
		String h = g.replaceAll("h", RAISED + RAISED + LETTER_SPACER);
		String i = h.replaceAll("i", RAISED + UNRAISED + LETTER_SPACER);
		String j = i.replaceAll("j", RAISED + RAISED + LETTER_SPACER);
		String k = j.replaceAll("k", UNRAISED + UNRAISED + LETTER_SPACER);
		String l = k.replaceAll("l", RAISED + UNRAISED + LETTER_SPACER);
		String m = l.replaceAll("m", UNRAISED + UNRAISED + LETTER_SPACER);
		String n = m.replaceAll("n", UNRAISED + RAISED + LETTER_SPACER);
		String o = n.replaceAll("o", UNRAISED + RAISED + LETTER_SPACER);
		String p = o.replaceAll("p", RAISED + UNRAISED + LETTER_SPACER);
		String q = p.replaceAll("q", RAISED + RAISED + LETTER_SPACER);
		String r = q.replaceAll("r", RAISED + RAISED + LETTER_SPACER);
		String s = r.replaceAll("s", RAISED + UNRAISED + LETTER_SPACER);
		String t = s.replaceAll("t", RAISED + RAISED + LETTER_SPACER);
		String u = t.replaceAll("u", UNRAISED + UNRAISED + LETTER_SPACER);
		String v = u.replaceAll("v", RAISED + UNRAISED + LETTER_SPACER);
		String w = v.replaceAll("w", RAISED + RAISED + LETTER_SPACER);
		String x = w.replaceAll("x", UNRAISED + UNRAISED + LETTER_SPACER);
		String y = x.replaceAll("y", UNRAISED + RAISED + LETTER_SPACER);
		String z = y.replaceAll("z", UNRAISED + RAISED + LETTER_SPACER);
		z=z.trim();
		return z;
	}

	public static String translateBottomLine(String top) {
		String tep = top.toLowerCase();
		String tup = tep.replaceAll(" ", WORD_SPACER);
		String a = tup.replaceAll("a", UNRAISED + UNRAISED + LETTER_SPACER);
		String b = a.replaceAll("b", UNRAISED + UNRAISED + LETTER_SPACER);
		String c = b.replaceAll("c", UNRAISED + UNRAISED + LETTER_SPACER);
		String d = c.replaceAll("d", UNRAISED + UNRAISED + LETTER_SPACER);
		String e = d.replaceAll("e", UNRAISED + UNRAISED + LETTER_SPACER);
		String f = e.replaceAll("f", UNRAISED + UNRAISED + LETTER_SPACER);
		String g = f.replaceAll("g", UNRAISED + UNRAISED + LETTER_SPACER);
		String h = g.replaceAll("h", UNRAISED + UNRAISED + LETTER_SPACER);
		String i = h.replaceAll("i", UNRAISED + UNRAISED + LETTER_SPACER);
		String j = i.replaceAll("j", UNRAISED + UNRAISED + LETTER_SPACER);
		String k = j.replaceAll("k", RAISED + UNRAISED + LETTER_SPACER);
		String l = k.replaceAll("l", RAISED + UNRAISED + LETTER_SPACER);
		String m = l.replaceAll("m", RAISED + UNRAISED + LETTER_SPACER);
		String n = m.replaceAll("n", RAISED + UNRAISED + LETTER_SPACER);
		String o = n.replaceAll("o", RAISED + UNRAISED + LETTER_SPACER);
		String p = o.replaceAll("p", RAISED + UNRAISED + LETTER_SPACER);
		String q = p.replaceAll("q", RAISED + UNRAISED + LETTER_SPACER);
		String r = q.replaceAll("r", RAISED + UNRAISED + LETTER_SPACER);
		String s = r.replaceAll("s", RAISED + UNRAISED + LETTER_SPACER);
		String t = s.replaceAll("t", RAISED + UNRAISED + LETTER_SPACER);
		String u = t.replaceAll("u", RAISED + RAISED + LETTER_SPACER);
		String v = u.replaceAll("v", RAISED + RAISED + LETTER_SPACER);
		String w = v.replaceAll("w", UNRAISED + RAISED + LETTER_SPACER);
		String x = w.replaceAll("x", RAISED + RAISED + LETTER_SPACER);
		String y = x.replaceAll("y", RAISED + RAISED + LETTER_SPACER);
		String z = y.replaceAll("z", RAISED + RAISED + LETTER_SPACER);
	z=z.trim();
		return z;
	}
}
