package hw7;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public class WebUtilities {

	public static String slurpURL(URL addr) throws IOException {

		URLConnection conn = addr.openConnection();
		InputStream is = conn.getInputStream();
		Scanner inner = new Scanner(is);
		String s = "";
		while (inner.hasNextLine())
		{
			s += inner.nextLine();
		}
		inner.close();
		return s;
	}
}
