package hw7;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

public class Place {
	private String name;
	private double latitude;
	private double longitude;
	private Set persons;

	public String getName() {
		return name;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;

	}

	public Place(String s, double lat, double longt) {
		this.name = s;
		this.latitude = lat;
		this.longitude = longt;
		this.persons = new Set();
	}

	public URL toGeocodeURL() throws MalformedURLException, URISyntaxException {
		String s = name.replaceAll(" ", "%20");
		URL url = new URL("http://www.twodee.org/teaching/cs145/2016c/homework/hw7/geocode.php?place=" + s);
		return url;

	}

	// Place newPlace = new Place(constructor);
	public Place(String s) throws NoSuchPlaceException, IOException, IOException, URISyntaxException {
		this.persons = new Set();
		this.name = s;
		String str = WebUtilities.slurpURL(toGeocodeURL());
		System.out.println(str);
		try {
			Scanner in = new Scanner(str);
			double lat = Double.parseDouble(in.next());
			double ong = Double.parseDouble(in.next());
			this.latitude = lat;
			this.longitude = ong;
			in.close();
		} catch (Exception E) {
			throw new NoSuchPlaceException(str);
		}
	}

	public void addPerson(String s) {
		if (!persons.has(s));
		persons.add(s);
	}

	public String toKML() {
		String places = "<Placemark>" + System.lineSeparator() + "<name>" + this.name + "</name>"
				+ System.lineSeparator() + "<description>" + this.persons + "</description>" + System.lineSeparator()
				+ "<Point><coordinates>" + String.format("%.2f", this.longitude) + "," +  String.format("%.2f", this.latitude) + "</coordinates></Point>"
				+ System.lineSeparator() + "</Placemark>";
		return places;
	}
}
