package hw7;

import java.util.ArrayList;

public class Set {

	ArrayList<String> set;

	public Set() {
		this.set = new ArrayList<String>();
	}

	public boolean has(String s) {
		return set.contains(s);
	}

	public void add(String s) {
		if (!has(s)) {
			set.add(s);
		}
	}

	public String toString() {
		String superString = "";
		for (int i = 0; i < set.size() - 1; i++) {
			String s = set.get(i);
			superString += s + System.lineSeparator();
		}
		String st = set.get(set.size() - 1);
		superString += st;
		return superString;
	}
}
