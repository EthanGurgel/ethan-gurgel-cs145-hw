package hw7;

public class NoSuchPlaceException extends Exception {

	private static final long serialVersionUID = 1L;

	
	public NoSuchPlaceException(String s) {
		super(s);
	}
}
