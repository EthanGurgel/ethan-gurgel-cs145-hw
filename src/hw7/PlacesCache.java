package hw7;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class PlacesCache {
	private ArrayList<Place> places;

	public PlacesCache() {
		this.places = new ArrayList<Place>();
	}

	public boolean isCached(String s) {
		for (int i = 0; i < places.size(); i++) {
			if (s.equals(places.get(i).getName())) {
				return true;
			}
		}
		return false;
	}

	public Place getPlace(String s) throws NoSuchPlaceException, IOException, URISyntaxException {
		if (isCached(s)) {
			int t = 0;
			while (!places.get(t).getName().equals(s)) {
				t++;
			}
			return places.get(t);
		}
		/*
		 * int i = places.indexOf(s); return places.get(i); } else { Place place
		 * = new Place(s); places.add(place); return place; }
		 */
		Place place = new Place(s);
		places.add(place);
		return place;
	}

	public int size() {

		return places.size();
	}

	public Place get(int i) throws IndexOutOfBoundsException {
		return places.get(i);
	}
}
