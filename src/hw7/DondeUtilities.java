package hw7;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Scanner;

public class DondeUtilities {


	public static PlacesCache readCSV(File f)
			throws FileNotFoundException, URISyntaxException, IOException {
		PlacesCache pl = new PlacesCache();
		Scanner in = new Scanner(f);
		while (in.hasNextLine()) {
			try {
				String s = in.nextLine();
				String[] a = s.split("\\|", -1);
				pl.getPlace(a[1]).addPerson(a[0]);
			} catch (NoSuchPlaceException er) {
				System.err.println("File was not located.");
			}
		}
		in.close();
		return pl;
	}

	public static void writeKML(PlacesCache cache, File f) throws FileNotFoundException {
		PrintWriter pr = new PrintWriter(f);
		pr.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		pr.println("<kml xmlns=\"http://www.opengis.net/kml/2.2\">");
		pr.println("<Document>");
		pr.println("<name>Donde</name>");
		for (int i = 0; i < cache.size(); i++) {
			pr.println(cache.get(i).toKML());
		}

		pr.println("</Document>");
		pr.println("</kml>");
		pr.close();
	}
}
